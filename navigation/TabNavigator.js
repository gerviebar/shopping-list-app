import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MainStackNavigator from './StackNavigator';
import { Ionicons } from '@expo/vector-icons';
import Archive from '../screens/Archive';

const Tab = createBottomTabNavigator();

export default function TabNavigator() {

    const screenOptions = (route, color) => {
        let iconName;
      
        switch (route.name) {
          case 'Shopping List':
            iconName = 'list';
            break;
          case 'Archive':
            iconName = 'archive';
            break;
          default:
            break;
        }
        return <Ionicons name={iconName} color={color} size={24} />;
      };

    return (
        <Tab.Navigator
          tabBarOptions={{activeTintColor: '#867AE9'}}
          screenOptions={({route}) => ({
            tabBarIcon: ({color}) => screenOptions(route, color)
        })}>     
            <Tab.Screen name='Shopping List' component={MainStackNavigator} />
            <Tab.Screen name='Archive' component={Archive} />
        </Tab.Navigator> 
    )
}
