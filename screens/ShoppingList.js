import React, { useState } from 'react'
import { StyleSheet, Text, View, Modal, TouchableOpacity, FlatList, Image, Alert } from 'react-native';
import { AntDesign, MaterialCommunityIcons } from '@expo/vector-icons';
import { editList, addToArchive, deleteList, clearList } from '../redux/actions/listAction'
import { setConfirmVisible } from '../redux/actions/boolAction'
import { formatDate, descendingOrder, ascendingOrder, showToastNotification } from '../services'
import Dialog from "react-native-dialog";
import { connect } from 'react-redux';

function ShoppingList(props) { 
    const [isOptionsVisible, setIsOptionsVisible] = useState(false)
    const [storeValue, setStoreValue] = useState('')
    const [newTitle, setNewTitle ] = useState('')
    const [saveList, setSaveList] = useState({})

    const { 
        navigation, 
        allList, 
        archiveList, 
        editListTitle, 
        addListToArchive, 
        deleteMyList, 
        clearAllList, 
        isAscending, 
        isConfirmVisible,
        confirmVisibilty 
    } = props

    // handle edit list title
    const handleEditTitle = (id, newTitle) => {
        newTitle.length === 0
        ? [Alert.alert('Please enter the title'), confirmVisibilty(true)]
        : [editListTitle(id, newTitle), showToastNotification(storeValue), setNewTitle(''), hideModals()]
    }

    // handle archive
    const handleArchive = (action) => {
        setStoreValue(action)
        archiveList.filter(item => item.id === saveList.id).length > 0
            ? Alert.alert('List is already in the Archive')
            : [addListToArchive(saveList), showToastNotification(storeValue), hideModals()]   
    }

    // hide modals
    const hideModals = () => {
        confirmVisibilty(false);
        setIsOptionsVisible(false)   
    }

    // handle clear, delete, and edit 
    const handleConfirmAction = () => {
        storeValue === 'clear'
        ? [clearAllList(), showToastNotification(storeValue), hideModals()]
        : storeValue ==='delete'
        ? [deleteMyList(saveList.id), showToastNotification(storeValue), hideModals()]
        : handleEditTitle(saveList.id, newTitle)              
    }

    // open confirmation dialog
    const openConfirmDialog = (action) => {
        confirmVisibilty(true)
        setStoreValue(action)
    }

    // open options modal
    const openOptions = (list) => {
        setSaveList(list)
        setIsOptionsVisible(true);
    }

    return (
        <View style={styles.container}>
            {allList === undefined || allList.length == 0
                ? <View style={styles.emptyWrapper}>
                    <Image
                        source={require('../assets/shopping-bag.png')}
                        alt={'shopping bag icon'}
                        style={styles.bagIcon}
                    />
                    <Text style={styles.emptyLabel}>Create Your Shopping List</Text>
                    <Text style={styles.emptyText}>Your current list is empty.</Text>
                </View>
                :
                <FlatList
                    data={isAscending? ascendingOrder(allList) : descendingOrder(allList)}
                    renderItem={({ item }) => {
                        return (
                            <View style={styles.listContainer}>
                                <TouchableOpacity onPress={() => navigation
                                    .navigate('Details', item)}>
                                    <View style={styles.row}>

                                        <Text style={styles.titleName}>{item.title}</Text>

                                        <TouchableOpacity onPress={() => openOptions(item)}>
                                            <MaterialCommunityIcons name="dots-vertical" size={24} color="#000" style={styles.dotsIcon} />
                                        </TouchableOpacity>

                                        <Modal transparent={true} visible={isOptionsVisible}>
                                            <View style={styles.optionContainer}>

                                                {/* edit title */}
                                                <TouchableOpacity onPress={() => openConfirmDialog('edit')}>
                                                    <Text style={styles.optionText}>Edit</Text>
                                                </TouchableOpacity>                                               

                                                {/* delete list */}
                                                <TouchableOpacity onPress={() => openConfirmDialog('delete')}>
                                                    <Text style={styles.optionText}>Delete</Text>
                                                </TouchableOpacity>                                              
                                                
                                                {/* add to archive */}
                                                <TouchableOpacity onPress={() => handleArchive('archive')}>
                                                    <Text style={styles.optionText}>Archive</Text>
                                                </TouchableOpacity>

                                                {/* clear list */}
                                                <TouchableOpacity onPress={() => openConfirmDialog('clear')}>
                                                    <Text style={styles.optionText}>Clear All</Text>
                                                </TouchableOpacity>

                                                {/* close button */}
                                                <TouchableOpacity onPress={() => setIsOptionsVisible(false)}>
                                                    <Text style={styles.optionText}>Cancel</Text>
                                                </TouchableOpacity>

                                            </View>
                                        </Modal>                                   
                                    </View>

                                    {/* confirm delete modal */}
                                    <View>
                                        <Dialog.Container visible={isConfirmVisible}>
                                            <>
                                                <Dialog.Title style={styles.dialogTitle}>
                                                    {storeValue === 'edit'? `Edit Title`
                                                    : storeValue === 'delete'? "Delete Shopping List" 
                                                    : "Clear All Shopping List" }
                                                </Dialog.Title>
                                                {  storeValue === 'edit'
                                                    ? <Dialog.Input 
                                                        placeholder="New Title"
                                                        onChangeText={text => setNewTitle(text)} 
                                                        style={styles.edit}/>
                                                    : <Dialog.Description style={styles.dialogDesc}>
                                                        Are you sure? You cannot undo this action.
                                                    </Dialog.Description>
                                                }  
                                            </>
    
                                            <Dialog.Button 
                                                label="Cancel" 
                                                onPress={() => confirmVisibilty(false)} />
                                            <Dialog.Button 
                                                label={storeValue === 'edit'? 'Submit'
                                                    : storeValue === 'clear' ? 'Clear' 
                                                    : 'Delete' } 
                                                onPress={handleConfirmAction} />
                                        </Dialog.Container>
                                    </View>

                                    {/* shopping list */}
                                    <View style={styles.row}>
                                        <View style={styles.itemListWrapper}>
                                            {item.items.length ?
                                                item.items.slice(0, 3).map((el, idx, arr) => {
                                                    return <Text
                                                        style={styles.itemList}
                                                        key={idx}>
                                                        {el.value.trim()}{idx !== arr.length - 1 ? ', ' : null}
                                                    </Text>
                                                }) :
                                                <Text style={styles.itemList}>No items yet</Text>
                                            }
                                        </View>
                                        <Text style={styles.date}>{formatDate(item.date)}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        )
                    }}
                    keyExtractor={(item) => item.id}
                    style={styles.listItem}
                >
                </FlatList>
            }

            <View style={styles.plusIcon}>
                <AntDesign name='pluscircle' size={38} color='#867AE9'
                    onPress={() => navigation.navigate('Create List')} />
            </View>
        </View>
    )
}

const mapStateToProps = state => {
    return {
        isAscending: state.bool.isAscending,
        allList: state.list.allList,
        archiveList: state.list.archiveList,
        isConfirmVisible: state.bool.isConfirmVisible
    }
}
  
const mapDispatchToProps = dispatch => {
    return {
        editListTitle: (id, title) => dispatch(editList(id, title)),
        addListToArchive: (list) => dispatch(addToArchive(list)),
        deleteMyList: (id) => dispatch(deleteList(id)),
        clearAllList: () => dispatch(clearList()),
        confirmVisibilty: (bool) => dispatch(setConfirmVisible(bool))
    }
}
  
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        position: 'relative',
        width: '100%'
    },
    confirmContainer: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
        position: 'absolute'
    },
    text: {
        color: '#000',
        marginTop: 60,
        backgroundColor: '#867AE9',
        fontSize: 20,
        color: '#fff',
        width: '100%',
        textAlign: 'left',
        padding: 10,
    },
    plusIcon: {
        position: 'absolute',
        bottom: 30,
        right: 30,
        alignSelf: 'flex-end'
    },
    listItem: {
        width: '100%',
    },
    listContainer: {
        width: '100%',
        padding: 10,
    },
    optionContainer: {
        position: 'absolute',
        top: '50%',
        width: '100%',
        height: '50%',
        backgroundColor: '#867AE9',
        padding: 20,
    },
    optionText: {
        paddingBottom: 20,
        fontSize: 18,
        fontWeight: 'bold',
        borderBottomWidth: 1,
        borderColor: '#E8E8E8',
        textAlign: 'center',
        color: '#fff',
        margin: 10,
    },
    closeOption: {
        color: '#21094E'
    },
    titleName: {
        fontSize: 20,
        fontWeight: 'bold',
        textTransform: 'capitalize',
        color: '#867AE9',
        marginTop: 2,
        marginLeft: 5
    },
    itemListWrapper: {
        display: 'flex',
        flexDirection: 'row',
        marginLeft: 5,
    },
    itemList: {
        textTransform: 'capitalize',
        fontStyle: 'italic',
        color: '#63686E',
    },
    row: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        backgroundColor: '#E6E6E6',
        padding: 6
    },
    date: {
        marginRight: 15,
        marginTop: 2,
        color: '#BCBAB8'
    },
    emptyWrapper: {
        alignItems: 'center',
        marginTop: 150,
        justifyContent: 'center',
    },
    bagIcon: {
        marginBottom: 10,
    },
    emptyText: {
        fontSize: 16,
    },
    emptyLabel: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    dotsIcon: {
        marginTop: 2,
        marginRight: 5
    }, 
    edit: {
        marginTop: 15,
        fontSize: 16,
        paddingBottom: 10
    },
    dialogTitle: {
        marginLeft: 10
    },
    dialogDesc: {
        marginLeft: 10
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingList)


