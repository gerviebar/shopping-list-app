import { ToastAndroid } from 'react-native';

const monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", 
                    "September", "October", "November", "December" ];
const arrOfColors = ["#B5EAEA", "#E7D4B5", "#FED049", "#C9CBFF", "#BBBBBB", "#FCF7BB", "#C886E5", "#BB8FA9"]                  

 //format date
export const formatDate = (date) => {
const dateToConvert = new Date(date)
const converted = `${monthNames[dateToConvert.getMonth()]} ${dateToConvert.getDate()}, ${dateToConvert.getFullYear()}`
return converted;
}

// descending order 
export const descendingOrder = (arr) => {
    const sortedDesc = arr.sort((a, b) => {
        const aDate = new Date(a.date)
        const bDate = new Date(b.date)
        return bDate.getTime() - aDate.getTime()
      })
    return sortedDesc;
}

// ascending order
export const ascendingOrder = (arr) => {
    const sortedAscend = arr.sort((a, b) => {
        const aDate = new Date(a.date)
        const bDate = new Date(b.date)
        return aDate.getTime() - bDate.getTime() 
      })
    return sortedAscend;
}

 // random color
export function random() {
    const random = arrOfColors[Math.floor(Math.random()*arrOfColors.length)];
    return random
}

  //new date
export function newDate() {
    const today = new Date().toString()
    return today;
} 

// toast notification message
const showToastWithGravity = (action) => {
    ToastAndroid.showWithGravity(
        `You have successfully ${action === 'archive'
        ? 'added shopping list to Archive!' 
        : action === 'delete'
        ? 'deleted the shopping list' 
        : action === 'clear'
        ? 'cleared all shopping List'
        : 'changed the title'}`,
        ToastAndroid.SHORT,
        ToastAndroid.TOP
    )
};

// toast notification
export const showToastNotification = (storeValue) => {
    setTimeout(() => {
        showToastWithGravity(storeValue)
    }, 500)
}