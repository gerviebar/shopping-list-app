import handleCreate from '../screens/CreateList'

describe('Component: CreateList', () => {
    describe('renders create list form', () => {
        it(" should render header title without crashing", () => {
            const header = <Text style={styles.createText}>CREATE A LIST</Text> 
            expect(wrapper.contains(header)).toBe(true); 
        })
        
        it('should have title textinput', () => { 
            const titleInput = 
            <TextInput 
                name='title'
                placeholder='Add Title...'
                underlineColorAndroid='transparent'
                keyboardType="visible-password"
                style={styles.titleInput} 
                onChangeText={text => setShoppingList({...shoppingList, title: text })}
            /> 
            expect(wrapper.find(titleInput).length).toEqual(1);
        });

        it('should contain add button', () => {
            const button = <Button name="create" style={styles.create}>CREATE</Button>
            expect(wrapper.containsMatchingElement(button)).toBe(true)
        })

        it('should contain create text button', () => {
            <Text style={styles.create}>CREATE</Text>
            expect(wrapper.containsMatchingElement(button)).toBe(true)
        })
    })

    // describe('when textinput is empty', () => {
    //     beforeEach(() => {

    //     })
    //     it('should not call add button', () => {
            
    //     })

    //     it('should not call submit button', () => {

    //     })

    // })

    // describe('when title matches added items', () => {
    //     beforeEach(() => {
            
    //     })
    // })
})
