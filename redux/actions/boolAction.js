export const ASCENDING = 'ASCENDING'
export const CONFIRM_ACTIONS = 'CONFIRM_ACTIONS'
export const MODAL_VISIBLE = 'MODAL_VISIBLE'

export const setAscending = () => {
    return {
        type: ASCENDING
    }
}

export const setConfirmVisible = (bool) => {
    return {
        type: CONFIRM_ACTIONS,
        payload: bool
    }
}

export const setModalVisible = (bool) => {
    return {
        type: MODAL_VISIBLE,
        payload: bool
    }
}

