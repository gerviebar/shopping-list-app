import { ASCENDING, CONFIRM_ACTIONS, MODAL_VISIBLE} from "../actions/boolAction";

const initialState = {
    isAscending: false,
    isConfirmVisible: false,
    isModalVisible: false
}
const boolReducer = ( state = initialState, action ) => {
    switch(action.type) {
        case ASCENDING:
            return {
                ...state, isAscending: !state.isAscending
            }

        case CONFIRM_ACTIONS:
            return {
                ...state, isConfirmVisible: action.payload
            }

        case MODAL_VISIBLE:
            return {
                ...state, isModalVisible: action.payload
            }

        default:
            return state;
    }
}

export default boolReducer;