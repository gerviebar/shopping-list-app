import React, { useState } from 'react'
import { StyleSheet, Text, View, Image, SafeAreaView, FlatList, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { clearArchive, removeArchive } from '../redux/actions/listAction'
import { setModalVisible } from '../redux/actions/boolAction';
import { formatDate, descendingOrder } from '../services'
import { Entypo } from '@expo/vector-icons'; 
import Dialog from "react-native-dialog";

function Archive({ archiveList, clearArchiveList, removeArchiveList, setModalVisibility, isModalVisible }) {
  const [saveListId, setSaveListId] = useState('')
  const [action, setAction] = useState('')

  //handle save delete data
  const saveActionDelete = (list_id) => {
    setSaveListId(list_id)
    setAction('remove')
    setModalVisibility(true)
  }

  //handle save clear data
  const saveActionClear = () => {
    setAction('clear')
    setModalVisibility(true)
  }

  //handle confirm delete/clear
  const handleConfirmAction = () => {
    action === 'clear'
    ? [clearArchiveList(), setModalVisibility(false)]
    : [removeArchiveList(saveListId), setModalVisibility(false)]
  }

  return (
      <SafeAreaView style={styles.container}>
          { archiveList === undefined || archiveList.length === 0
          ? <View style={styles.emptyWrapper}>
              <Image 
                    source={require('../assets/empty-box.png')} 
                    alt={'empty icon'} 
                    style={styles.emptyIcon} 
                />

                <Text style={styles.emptyLabel}>No Archive, yet</Text>
                <Text style={styles.emptyText}>There are no lists in your archive.</Text>
            </View>
          : 
          <View style={{ flex: 1}}>
            <View style={styles.headerWapper}>
              <View style={styles.row}>
                <Text style={styles.header}>Archive</Text>         

                <Image 
                  source={require('../assets/archive-icon.png')} 
                  alt={'archive image'} 
                  style={styles.archiveImg} 
                />
              </View>   

                {/* clear all */}
               <TouchableOpacity onPress={saveActionClear}>
                  <Text style={styles.clearArchive}>Clear Archive</Text>
                </TouchableOpacity>            
            </View>
            <FlatList data={descendingOrder(archiveList)}
              renderItem={({ item }) => {
                return (

                    <View key={item.id} style={[styles.singleList, {backgroundColor: item.color}]}>
                        <View style={styles.rowList}>
                          <View style={styles.rowList}>
                            <Text style={styles.title}>{item.title}</Text>
                            <Text style={styles.itemQty}>(
                              {item.items.filter(el => el.completed === true).length}/
                              {item.items.length} {item.items.length > 1? 'items' : 'item'})</Text>
                          </View> 
                          <TouchableOpacity onPress={() => saveActionDelete(item.id)} style={styles.trashIcon}>
                            <Entypo name="trash" size={24} color="black" />
                          </TouchableOpacity>                                                                
                        </View>
                        <Text style={styles.date}>{formatDate(item.date)}</Text>  
                        <View style={styles.itemsRow}>               
                            {item.items.map(el => {
                              return (
                                <View key={el.id} style={styles.valueWrapper}>
                                  <Text style={[styles.value, 
                                    el.completed? {textDecorationLine: 'line-through', color: '#867AE9'}
                                    : {textDecorationLine: 'none', color: '#000'} ]}>{el.value}
                                  </Text>
                                  <Text></Text>
                                </View>
                              )})                          
                            }
                        </View> 
                    </View>
                 
                )
              }}
              keyExtractor={(item) => item.id}
              style={styles.archiveList}
              >                   
            </FlatList> 
             <Dialog.Container visible={isModalVisible}>
                <Dialog.Title style={styles.dialogTitle}>{action === 'clear' ? 'Clear All' : 'Remove from'} Archive
                </Dialog.Title>
                <Dialog.Description style={styles.dialogDesc}>Are you sure? You cannot undo this action.
                </Dialog.Description>
                  <Dialog.Button 
                    label={'Cancel'} 
                    onPress={() => setModalVisibility(false)} 
                  />
                  <Dialog.Button 
                    label={action === 'clear' ? 'Clear' : 'Delete'} 
                    onPress={handleConfirmAction} 
                    style={{ color: 'red'}}
                  />
              </Dialog.Container>
          </View>
          }   
      </SafeAreaView>
  )
}

const mapStateToProps = state => {
  return {
    archiveList: state.list.archiveList,
    isModalVisible: state.bool.isModalVisible

  }
}
    
const mapDispatchToProps = dispatch => {
    return {
      removeArchiveList: (list_id) => dispatch(removeArchive(list_id)),
      clearArchiveList: () => dispatch(clearArchive()),
      setModalVisibility: (bool) => dispatch(setModalVisible(bool))
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      position: 'relative'
    },
    headerWapper: {
      marginTop: 40,
    },
    header: {
      textAlign: 'center',
      color: '#000',
      fontSize: 25,
      fontWeight: 'bold'
    },
    row: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
    },
    text: {
      textAlign: 'center',
      color: '#000',
      fontSize: 18
    },
    archiveWrapper: {
      width: '100%',
    },
    archiveImg: {
      height: 35,
      width: 35,
      marginLeft: 6
    },
    singleList: {
      marginLeft: 20,
      marginRight: 20,
      borderRadius: 10,
      padding: 10,
      position: 'relative',
      marginTop: 10,
      marginBottom: 10
    },
    rowList: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center'
    },
    itemsRow: {
      marginTop: 20
    },
    title: {
      fontSize: 18,
      textAlign: 'left',
      marginTop: 10,
      fontWeight: 'bold',
      fontSize: 20,
      marginLeft: 10
    },
    value: {
      marginLeft: 10,
    },
    emptyWrapper: {
      marginTop: 60
    },
    emptyIcon: {
      alignSelf: 'center',
      marginTop: 160,
      marginBottom: 20
    },
    emptyText: {
      fontSize: 16,
      textAlign: 'center'
    },
    emptyLabel: {
      fontSize: 20,
      textAlign: 'center',
      fontWeight: 'bold'
    },
    date: {
      color: '#707070',
      marginLeft: 10,
      fontSize: 13
    },
    itemQty: {
      marginLeft: 10,
      marginTop: 10,
      color: '#4C4C4C'
    },
    removeArchiveIcon: {
      position: 'absolute',
      top: -20,
      left: '90%',
      width: 36,
      height: 36
    },
    clearArchive: {
      textAlign: 'right',
      marginRight: 20
    },
    dialogTitle: {
      marginLeft: 10,
      fontWeight: 'bold'
    },
    dialogDesc: {
      marginLeft: 10,
    }
  });

  export default connect(mapStateToProps, mapDispatchToProps)(Archive)
  
