import React, { useState, useRef, useEffect } from 'react'
import { v4 as uuid } from 'uuid'
import { Ionicons } from '@expo/vector-icons'
import { connect } from 'react-redux'
import { createList } from '../redux/actions/listAction'
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Alert, Image, ScrollView, SafeAreaView } from 'react-native';
import { random, newDate } from '../services'

function CreateList({ navigation, createMyList }) {
    const [value, setValue] = useState('')
    const [shoppingList, setShoppingList] = useState({
        title: '',
        id: uuid(),
        date: newDate(),
        color: random(),
        items: [],           
    }) 

    const scrollViewRef = useRef();

    useEffect(() => {
        random();
        newDate();
     }, []) 

     
    //creating shopping list
    const handleCreate = e => {
        e.preventDefault();

        if (shoppingList.title.length === 0) {
            Alert.alert('shopping list title is required')
        } else {
            createMyList(shoppingList)           
            navigation.goBack();
        } 
    }
    
    //add item to shoppinglist 
    const addValue = () => {
        if (value.length === 0 ) {
            Alert.alert('Please enter the item')
        } else {
            const result = shoppingList.items
              .filter(obj => obj.value.toLowerCase() === value.toLowerCase())
            
            result.length !== 0
            ? Alert.alert('Item is already in the list')
            : setShoppingList({ 
                ...shoppingList, 
                items: [ ...shoppingList.items, {value, id: uuid(), completed: false}]
            }) 
        } 
      }

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView
                ref={scrollViewRef}               
                onContentSizeChange={() => scrollViewRef.current.scrollToEnd({ animated: true })}
            >
                <View transparent={true} style={styles.createListContainer}>          
                            <View style={styles.form}>
                                <Ionicons name="arrow-back-sharp" size={24} color="#fff"
                                    onPress={() => navigation.goBack()}
                                    style={styles.gobackBtn} 
                                />

                                <View style={styles.header}>  
                                    <Text style={styles.createText}>CREATE A LIST</Text>                          
                                </View>
                                
                                <Image 
                                    source={require('../assets/pencil-icon.png')} 
                                    alt={'pencil icon'} 
                                    style={styles.pencilImg} 
                                />

                                <View style={styles.topBox}>
                                    <TextInput 
                                        placeholder='Add Title...'
                                        underlineColorAndroid='transparent'
                                        keyboardType="visible-password"
                                        style={styles.titleInput} 
                                        onChangeText={text => setShoppingList({...shoppingList, title: text })}
                                    /> 

                                </View>
                                    
                                <View style={styles.bottomBox}>
                                    <View style={styles.itemWrapper}>
                                        <TextInput 
                                            placeholder='Add Item..'
                                            underlineColorAndroid='transparent'
                                            keyboardType="visible-password"
                                            style={styles.itemInput}
                                            onChangeText={text => setValue(text)} 
                                        />

                                        <TouchableOpacity style={styles.addWrapper} onPress={addValue}>
                                            <Text style={styles.addBtn}>ADD</Text>
                                        </TouchableOpacity>                             
                                    </View>
                                    
                                    {/* map through items */}
                                    {shoppingList.items.length !== 0 &&
                                    <View>
                                        {shoppingList.items.map(el => {
                                            return <View key={el.id}>
                                                <Text style={styles.itemText}>{el.value}</Text>
                                            </View>
                                        })}
                                    </View>
                                    }
                                    
                                </View>
                                
                                {/* create a list button */}
                                <TouchableOpacity style={styles.createBtn} onPress={handleCreate} >
                                    <Text style={styles.create}>CREATE</Text>
                                </TouchableOpacity>
                            </View>
                 </View>
            </ScrollView>
        </SafeAreaView>
    )
}
      
const mapDispatchToProps = dispatch => {
    return {
    createMyList: list => dispatch(createList(list))   
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#867AE9',
    },
    createListContainer: {
        padding: 15,
        width: '100%',
    },
    form: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 40
    },
    createText: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#fff'
    },
    gobackBtn: {
        alignSelf: 'flex-start',
        position: 'absolute',
        top: 0,
        left: 5,
        width: '100%'
    },
    titleInput: {
        backgroundColor: '#fff' ,
        padding: 8,
        borderRadius: 5,
        fontSize: 16,
        marginTop: 20,
        paddingLeft: 12,
    },
    itemWrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 4,
        marginBottom: 4,   
    },
    itemInput: {
        padding: 8,
        paddingBottom: 4,
        paddingLeft: 2,
        height: '100%',
        width: '60%',
        fontSize: 16,
        borderBottomColor: '#DDDDDD',
        marginRight: 10,
        marginLeft: 10,     
    },
    itemText: {
        textTransform: 'capitalize',
        backgroundColor: '#E1E8EB',
        color: '#000',
        padding: 8, 
        marginTop: 4,
        fontStyle: 'italic',
        color: '#867AE9'
    },
    topBox: {
        width: '80%',
        position: 'relative',
        marginTop: 10
    },
    bottomBox: {
        backgroundColor: '#fff',
        marginTop: 20,
        borderRadius: 5,
        width: '80%'
    },
    createBtn: {
        marginTop: 20,
        backgroundColor: '#DDDDDD',                  
        width: '80%'
    },
    create: {
        color: '#867AE9',
        fontSize: 20,
        textAlign: 'center',
        fontWeight: "bold",
        padding: 8,
    },
    addWrapper: {
        width: '25%',
        backgroundColor: '#867AE9',
        marginRight: 5
    },
    addBtn: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#fff',
        paddingTop: 7,
        paddingBottom: 7,
        textAlign: 'center',
    },
    clearTitleBtn: {
        position: 'absolute',
        top: '42%',
        right: '5%'
    },
    clearItemBtn: {
        position: 'absolute',
        top: '15%',
        right: '30%'
    },
    pencilImg: {
        height: 30,
        width: 30,
        marginTop: 5
    },
})

export default connect(null, mapDispatchToProps)(CreateList)