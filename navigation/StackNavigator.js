import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import ShoppingList from '../screens/ShoppingList';
import ListDetails from '../screens/ListDetails';
import CreateList from '../screens/CreateList';
import { FontAwesome } from '@expo/vector-icons'; 
import { TouchableHighlight } from 'react-native'
import { connect } from 'react-redux';
import { setAscending } from '../redux/actions/boolAction'

const Stack = createStackNavigator();

const screenOptionStyle = {
    headerStyle: {
      backgroundColor: '#867AE9',
    },
    headerTintColor: 'white',
    headerBackTitle: 'Back',
};


function MainStackNavigator({ setAscending }) {

    return (
            <Stack.Navigator screenOptions={screenOptionStyle}>
                <Stack.Screen 
                    name='Shopping Lists' 
                    component={ShoppingList} 
                    options={{ headerRight: () => (
                        <TouchableHighlight 
                            style={{marginRight: 20, paddingLeft: 10, paddingRight: 10, paddingTop: 2, paddingBottom: 2}} 
                            activeOpacity={0.6}  
                            underlayColor="#726A95" 
                            onPress={() => setAscending()}>
                                <FontAwesome 
                                    name="sort" 
                                    size={24} 
                                    color="black" 
                                />
                        </TouchableHighlight>
                    )}}
                />                  
                <Stack.Screen name="Details" component={ListDetails} />
                <Stack.Screen name="Create List" component={CreateList} options={{headerShown: false}} />
            </Stack.Navigator>
    )
}

const mapDispatchToProps = dispatch => {
    return {
        setAscending: () => dispatch(setAscending())
    }
}


export default connect(null, mapDispatchToProps)(MainStackNavigator)
