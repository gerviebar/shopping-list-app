import {
    CREATE_LIST,
    EDIT_LIST,
    DELETE_LIST,
    CLEAR_LIST,
    ADD_ITEM,
    TOGGLE_ITEM,
    DELETE_ITEM,
    ADD_TO_ARCHIVE,
    REMOVE_ARCHIVE,
    CLEAR_ARCHIVE
} from '../actions/listAction'

const initialState = {
    allList: [],
    archiveList: []
}

const updateListInItems = (array, action) => {

    if (array === undefined || array.length === 0) {
        return [action.value]
    } 

    return array.map( list=> {
        if (list.id !== action.listId) {
            return list
        }
    
        return {
            ...list,
            items: [ ...list.items, action.value]
        }
    })    
} 

const toggleItemsInList = (array, action) => {

    return array.map( list=> {
        return {
            ...list,
            items: list.items.map(item =>
                item.id === action.itemId
                  ? {
                      ...item,
                      completed: !item.completed,
                    }
                  : item
              )
         }
    })    
} 

const deleteItemFromArray = (arr, action) => {

    return arr.map( list => {
        if (list.id === action.listId)

        return {
            ...list,
            items: list.items.filter(item => item.id !== action.itemId)
        }
    })    
}


const listReducer = (state = initialState, action) => {
    switch(action.type) {

        case CREATE_LIST: {

            return {
                ...state,
                allList: [...state.allList, action.payload]
            }
        };

        case EDIT_LIST: {

            return {
                ...state,
                allList: state.allList.map(list => {
                            if (list.id === action.payload.id) {
                            return { ...list, title: action.payload.newTitle }
                            } 
                            return list
                        })
            }

        };

        case DELETE_LIST: {

            return {
                ...state,
                allList: state.allList.filter(list => list.id !== action.payload)
            }
        };

        case CLEAR_LIST: {

            return {
                ...state,
                allList: []
            }
        };

        case ADD_ITEM: {
    
           return {
                ...state,
                allList: updateListInItems( [ ...state.allList ], action.payload )
            }   
        };

        case TOGGLE_ITEM: { 

            return {
                ...state,
                allList: toggleItemsInList([ ...state.allList ], action.payload )
            }  
        };

        case DELETE_ITEM: {

            return {
                ...state,
                allList: deleteItemFromArray([ ...state.allList ], action.payload)
            }
        };

        case ADD_TO_ARCHIVE: {

            return {
                ...state,
                allList: state.allList.filter(list => list.id !== action.payload.id),
                archiveList: [ ...state.archiveList, action.payload]
            }
        };

        case REMOVE_ARCHIVE: {

            return {
                ...state,
                archiveList: state.archiveList.filter(list => list.id !== action.payload)
            }
        };

        case CLEAR_ARCHIVE: {

            return {
                ...state,
                archiveList: []
            }
        };

        default:
            return state;
    }
}

export default listReducer;
