import React, { useState, useEffect, useRef } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, TextInput, CheckBox, SafeAreaView, ScrollView } from 'react-native';
import { addItem, toggleItem, deleteItem } from '../redux/actions/listAction'
import { Ionicons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import Dialog from "react-native-dialog";

import { v4 as uuid } from 'uuid';

function ListDetails(props) {
    const [value, setValue] = useState('')
    const [storeValue, setStoreValue] = useState('')
    const [storeId, setStoreId] = useState('')
    const [isDialogVisible, setIsDialogVisible] = useState(false)
    const [currentList, setCurrentList] = useState({ ...props.route.params}) 
    const { allList, addNewItemToList, toggleItemFromList, deleteItemFromList, navigation } = props

    const scrollViewRef = useRef();

    useEffect(() => {
        changeHeader();
    }, [])

    //change Header
    const changeHeader = () => {
        navigation.setOptions({ headerTitle: props.route.params.title })
    }

    useEffect(()=>{
        const current = allList.find(list => list.id === currentList.id)
        setCurrentList(current);
    }, [allList]);
   
    
    //add item to list
    const handleAddItem = () => {
        if (value.length === 0) {
            setStoreValue('empty')
            setIsDialogVisible(true)
        } else { 
            currentList.items.some(item => item.value.toLowerCase() === value.toLowerCase())
            ? (setStoreValue('exist'), setIsDialogVisible(true))
            : addNewItemToList({value, id: uuid(), completed: false}, props.route.params.id)
        }                
    }

    //show confirm modal
    const showConfirmModal = (id, action) => {
        setStoreId(id)
        setStoreValue(action)
        setIsDialogVisible(true)
    }

    //check if current list is empty
    const isEmpty = (obj) => {
        for(let key in obj) {
            if(obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }

    //delete item
   const handleDelete = (list_id, item_id) => {
        deleteItemFromList(list_id, item_id)
        setIsDialogVisible(false)
    }

    return (
            <SafeAreaView style={styles.container}>
                <ScrollView
                    ref={scrollViewRef}               
                    onContentSizeChange={() => scrollViewRef.current.scrollToEnd({ animated: true })}
                >
                <View style={styles.addInputWrapper}>

                    {/* add item to list */}
                    <TextInput 
                        placeholder='Add Item...'
                        underlineColorAndroid='transparent'
                        keyboardType="visible-password"
                        style={styles.addInput} 
                        onChangeText={text => setValue(text)}
                    /> 

                    <Text style={styles.quantity}>
                        {currentList.items.filter(el => el.completed === true).length}/
                        {currentList.items.length} {currentList.items.length > 1? 'items' : 'item'}</Text>
                    
                    <TouchableOpacity onPress={handleAddItem}>
                        <Text style={styles.submitBtn}>ADD</Text>
                    </TouchableOpacity>
                </View>
                
                {!isEmpty(currentList) && 
                <View style={styles.detailContainer}>
                    {currentList.items.map(el => {
                        return (
                                <View style={styles.itemContainer} key={el.id}>
                                    <View style={styles.left}>

                                    {/* toggle item */}
                                    <CheckBox 
                                        value={el.completed}
                                        tintColors={{true: '#867AE9', false: '#fff'}} 
                                        onValueChange={() => toggleItemFromList(currentList.id, el.id)}
                                        style={styles.checkbox}
                                        />

                                        <Text style={[styles.itemName, 
                                            el.completed? {textDecorationLine: 'line-through', color: '#867AE9'} 
                                            : {textDecorationLine: 'none', color: '#000'}]}>
                                            {el.value}
                                        </Text>
                                    </View>

                                    {/* delete item */}
                                    <TouchableOpacity onPress={() => showConfirmModal(el.id, 'delete')}>
                                        <Ionicons name="md-trash-outline" size={24} color="black" />
                                    </TouchableOpacity>
                                </View>

                        )  
                    })}                      
                </View>
                }

                    {/* confirm dialog */}
                <View style={styles.confirmContainer}>
                    <Dialog.Container visible={isDialogVisible}>
                        {storeValue === 'delete'
                        ?   <>
                                <Dialog.Description style={styles.alertDeleteDesc}>
                                    Do you want to delete this item? You cannot undo this action.
                                </Dialog.Description>
                                <Dialog.Button style={styles.dialogBtn} label="Cancel" onPress={() => setIsDialogVisible(false)} />
                                <Dialog.Button style={styles.deleteBtn} label="Delete" 
                                    onPress={() => handleDelete(currentList.id, storeId)} 
                                />
                            </>
                        :  
                            // alert message
                            <> 
                            <Dialog.Title style={styles.alertTitle}>Alert Message</Dialog.Title>
                            <Dialog.Description style={styles.dialogDesc}>
                                {storeValue === 'empty'
                                ? 'Please enter the item'
                                : 'Item is already in your list'}
                            </Dialog.Description>
                            <Dialog.Button label="OK" style={styles.dialogBtn} onPress={() => setIsDialogVisible(false)} /> 
                        </>
                        }
                    </Dialog.Container>
                </View>
                </ScrollView>
            </SafeAreaView>
    )
}

const mapStateToProps = state => {
    return {
        allList: state.list.allList,
    }
}
    
const mapDispatchToProps = dispatch => {
    return {
        addNewItemToList: (value, list_id) => dispatch(addItem(value, list_id)),
        toggleItemFromList: (list_id, item_id) => dispatch(toggleItem(list_id, item_id)),
        deleteItemFromList: (list_id, item_id) => dispatch(deleteItem(list_id, item_id))
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    addInputWrapper: {
        marginLeft: '5%',
        marginRight: '5%',
        position: 'relative'
    }, 
    addInput: {
        borderBottomWidth: 1,
        borderBottomColor: '#DDDDDD',
        marginTop: 10,
        height: 40
    },

    detailContainer: {
        marginTop: 10
    },
    itemContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginLeft: '5%',
        marginRight: '10%',
        padding: 8
    },
    itemName: {
        textTransform: 'capitalize',
        textAlign: 'left',
        fontSize: 16,
        marginLeft: 10
    },
    checkBox: {
        width: 45,
        height: 45,
    },
    left: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    submitBtn: {
        textAlign: 'center',
        backgroundColor: '#867AE9',
        color: '#fff',
        paddingTop: 4,
        paddingBottom: 4,
        width: '30%',
        alignSelf: 'center',
        marginTop: 10,
        fontSize: 18,
        fontWeight: 'bold'
    },
    quantity: {
        textAlign: 'right',
        position: 'absolute',
        right: '5%',
        top: '25%'
    },
    dialogDesc: {
        marginTop: 20,
        marginBottom: 10,
        textAlign: 'center'
    },
    alertTitle: {
        fontWeight: 'bold',
        textAlign: 'center',
        justifyContent: 'flex-start',
        fontSize: 20
    },
    dialogBtn: {
        fontWeight: 'bold',
        fontSize: 16
    },
    alertDeleteDesc: {
        marginBottom: 10,
        marginTop: 0,
        fontSize: 18,
        textAlign: 'center'
    },
    deleteBtn: {
        color: 'red',
        fontWeight: 'bold',
        fontSize: 16
    }

})

export default connect(mapStateToProps, mapDispatchToProps)(ListDetails)



