import React from 'react';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux'
import { store, persistor } from './redux/store'
import { NavigationContainer } from "@react-navigation/native";
import TabNavigator from './navigation/TabNavigator'

export default function App() {

  return (     
        <Provider store={store} >
          <PersistGate loading={null} persistor={persistor}>
              <NavigationContainer>
                  <TabNavigator />
              </NavigationContainer>
          </PersistGate>
        </Provider>    
  );
}
