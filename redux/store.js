import { combineReducers } from 'redux'
import { createStore, applyMiddleware } from 'redux'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { persistStore, persistReducer } from 'redux-persist';
import thunk from 'redux-thunk'

//reducer
import listReducer from './reducers/listReducer'
import boolReducer from './reducers/boolReducer';

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    whitelist: ['archiveList', 'allList']
};

const rootReducer = combineReducers({
    list: persistReducer(persistConfig, listReducer),
    bool: boolReducer
})

export const store = createStore(rootReducer, applyMiddleware(thunk))
export const persistor = persistStore(store);
