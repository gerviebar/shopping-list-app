//List action types
export const CREATE_LIST = 'CREATE_LIST'
export const EDIT_LIST = 'EDIT_LIST'
export const DELETE_LIST = 'DELETE_LIST'
export const CLEAR_LIST = 'CLEAR_LIST'

//Item action types
export const ADD_ITEM = 'ADD_ITEM'
export const TOGGLE_ITEM = 'TOGGLE_ITEM'
export const DELETE_ITEM = 'DELETE_ITEM'

//ARCHIVE action types
export const ADD_TO_ARCHIVE = 'ADD_TO_ARCHIVE'
export const REMOVE_ARCHIVE = 'REMOVE_ARCHIVE'
export const CLEAR_ARCHIVE = 'CLEAR_ARCHIVE'

export const createList = list => {
    return {
        type: CREATE_LIST,
        payload: list
    }
}

export const editList = (listId, listTitle) => {
    return {
        type: EDIT_LIST,
        payload: {
            id: listId,
            newTitle: listTitle
        }
    }
}

export const deleteList = (id) => {
    return {
        type: DELETE_LIST,
        payload: id
    }
}

export const clearList = () => {
    return {
        type: CLEAR_LIST
    }
}

export const addItem = (val, id) => {
    return {
        type: ADD_ITEM,
        payload: {
            listId: id,
            value: val
        }
    }
}

export const toggleItem = (list_id, item_id) => {
    return {
        type: TOGGLE_ITEM,
        payload: {
            listId: list_id,
            itemId: item_id
        }
    }
}

export const deleteItem = (list_id, item_id) => {
    
    return {
        type: DELETE_ITEM,
        payload: {
            listId: list_id,
            itemId: item_id
        }
    }
}

export const addToArchive = (list) => {
    return {
        type: ADD_TO_ARCHIVE,
        payload: list
    }
}

export const removeArchive = (list_id) => {
    return {
        type: REMOVE_ARCHIVE,
        payload: list_id
    }
}

export const clearArchive = () => {
    return {
        type: CLEAR_ARCHIVE
    }
}



